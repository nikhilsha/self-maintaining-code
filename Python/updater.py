import json
import requests as r

rawVerURL = "https://gitlab.com/nikhilsha/self-maintaining-code/-/raw/main/update-tag.json"

def check() -> int:
    # 0 means that there wasn't an update

    # 1 means that there was an update and the updater was able to install it
    # successfully

    # 2 means that there is an update that the updater is not able to handle
    # (such as an update to the updater itself). The main script would
    # have the code to update the updater if this integer is returned

    # -1 ERR
    installDir = __file__.replace("/updater.py", "/")
    verFile = open(__file__.replace("/updater.py", "/internalver.json"), 'r')
    ver = json.load(verFile)["ver"]

    verFile.close(); del verFile

    try:
        versionData = r.get(rawVerURL).json()
    except r.JSONDecodeError:
        print("Invalid JSON sent by server")
        return -1

    if versionData["ver"] == ver:
        return 0
    elif not versionData["ver"] == ver and versionData["magntd"] == "minor":
        updates = versionData["updates"]
        for item in updates:
            fileData = r.get(f"https://gitlab.com/nikhilsha/self-maintaining-code/-/raw/main/Python/{item}").text
            file = open(f"{installDir}{item}", 'w')
            file.write(fileData)
            file.close()
        tmp = open(f"{installDir}internalver.json", 'w'); json.dump(versionData, tmp); tmp.close(); del tmp
        return 1